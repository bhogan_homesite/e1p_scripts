curl --location --request POST 'http://localhost:8086/v1/
/transfer' \
--header 'Accept: application/xml' \
--header 'Content-Type: application/xml' \
--header 'X-Request-Id: dc507153-7c74-4c5b-906c-dcf972e2b3ba' \
--header 'Authorization: Bearer {{COMPRATER_AUTH0_BEARER_TOKEN}}' \
--data-raw '<ACORD>
	<InsuranceSvcRq>
		<HomePolicyAddRq>
			<ItemIdInfo>
				<SystemId>9832c47b-1341-4cb0-9c5f-81804de64c97</SystemId>
			</ItemIdInfo>
		</HomePolicyAddRq>
	</InsuranceSvcRq>
</ACORD>
'