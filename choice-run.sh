#!/bin/bash

# this assumes you have all the projects in the same directory, and you will have to change the basepath variable.

# running this - 
# you may need to run chmod 755 choice-run.sh first
# then call it using ./choice-run.sh connect | connect_auto_servicing | connect_home_servicing | homesite_home | homesite_home_servicing | amfam_auto | amfam_auto_servicing | amfam_servicing | aah_servicing

basepath="/Users/bhogan/dev/p20/"

connect=("sapi-inquiry sapi-offers sapi-aah-auto-offers")
connect_auto_servicing=("sapi-inquiry sapi-offers sapi-aah-auto-offers sapi-servicing sapi-aah-servicing")
connect_home_servicing=("sapi-inquiry sapi-offers sapi-aah-home-offers sapi-servicing sapi-aah-servicing")

homesite_home=("sapi-inquiry sapi-offers sapi-hs-offers")
homesite_home_servicing=("sapi-inquiry sapi-offers sapi-hs-offers sapi-servicing sapi-hs-servicing")

amfam_auto=("sapi-inquiry sapi-offers sapi-amfm-auto-offers")
amfam_auto_servicing=("sapi-inquiry sapi-offers sapi-amfm-auto-offers sapi-servicing sapi-af-servicing")
amfam_servicing=("sapi-servicing sapi-af-servicing")
aah_servicing=("sapi-servicing sapi-aah-servicing")


function run() {
    applications="${1}"
    for application in ${applications[@]}; do
        jar=$(ls -tr $basepath$application/target/*.jar | head -1)
        osascript -e 'tell app "Terminal"
            do script "mvn -f '"$basepath"''"$application"'/pom.xml -Dorg.slf4j.simpleLogger.defaultLogLevel=WARN spring-boot:run -Dspring-boot.run.profiles=local " 
        end tell'
    done
}

if [ -n "${!1}" ]
then
    run "${!1}" 
else 
    echo "Invalid application name"
    echo "Valid applications are: connect | connect_auto_servicing | connect_home_servicing | homesite_home | homesite_home_servicing | amfam_auto | amfam_auto_servicing | amfam_servicing | aah_servicing"
fi
