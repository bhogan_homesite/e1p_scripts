#!/bin/bash

# this assumes you have all the projects in the same directory, and you will have to change the basepath variable.

# running this - 
# you may need to run chmod 755 e1p-pull.sh first
# then call it using ./e1p-pull.sh 

basepath="/Users/bhogan/dev/p20/"
applications=("e1p-comparative-rater sapi-e1p-auto-offers sapi-e1p-home-offers sapi-e1p-transfer sapi-inquiry sapi-offers sapi-transfer")

for application in ${applications[@]}; do
    git -C $basepath$application pull     
done
