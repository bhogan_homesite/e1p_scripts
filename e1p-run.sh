#!/bin/bash

# this assumes you have all the projects in the same directory, and you will have to change the basepath variable.

# running this - 
# you may need to run chmod 755 e1p-run.sh first
# then call it using ./e1p-run.sh 

basepath="/Users/bhogan/dev/p20/"
applications=("sapi-e1p-auto-offers sapi-e1p-home-offers sapi-e1p-transfer sapi-inquiry sapi-offers sapi-transfer")


##### Use java -jar to start the application ######
###################################################
function runmvn() {
    for application in ${applications[@]}; do
        jar=$(ls -tr $basepath$application/target/*.jar | head -1)
        osascript -e 'tell app "Terminal"
            do script "java -XX:MinRAMPercentage=25 -XX:MaxRAMPercentage=85  -Dspring.profiles.active=desktop -server -jar '"$jar"' "
        end tell'
    done
}

##### Use mvn to start the applications #####
############################################
function runjava() {
    for application in ${applications[@]}; do
        osascript -e 'tell app "Terminal"
            do script "mvn -f '"$basepath"''"$application"'/pom.xml -Dorg.slf4j.simpleLogger.defaultLogLevel=WARN spring-boot:run -Dspring-boot.run.profiles=desktop " 
        end tell'
    done
}

# # you can do it this way too, explicitly writing the mvn or java jar command.
# #osascript -e 'tell app "Terminal"
#     # do script "mvn -f /Users/bhogan/dev/p20/sapi-e1p-auto-offers/pom.xml -Dorg.slf4j.simpleLogger.defaultLogLevel=WARN spring-boot:run -Dspring-boot.run.profiles=desktop"
# #end tell'


if [ "$1" = "mvn" ]
then 
    runmvn
elif [ "$1" = "java" ]
then 
    runjava
else
    if [ "$1" != "" ] 
    then
        echo $1 "is not a valid parameter"
    fi
    echo "Do you want to run using java or mvn?"
fi
